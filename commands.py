import requests
import discord
from discord.ext import commands
from discord import Game
from discord.utils import find
class id_perms():
    def __init__(self,bot):
        self.bot = bot

    @commands.command()
    async def isave(self, msg):
        a = self.bot.get_channel('445673668337729536')
        b = await self.bot.get_message(a, "445898320272162816")
        str = " ".join(msg)
        c = b.content + " " + str + ","
        await self.bot.edit_message(b, c)

    @commands.command()
    async def iget(self):
        a = self.bot.get_channel('445673668337729536')
        b = await self.bot.get_message(a, "445898320272162816")
        c = b.content.split(",")


def is_a(ctx):
    if ctx.message.author.id == "243742080223019019":
        return True
    return False


class sState:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def innit(self, ctx):
        await self.bot.say("Innit")
        await self.bot.delete_message(ctx.message)

    @commands.command()
    async def rsave(self, *args):
        a = self.bot.get_channel('445673668337729536')
        b = await self.bot.get_message(a, "445898320272162816")
        str = " ".join(args)
        c = b.content + " " + str + ","
        await self.bot.edit_message(b, c)

    @commands.command()
    async def rstate(self):
        a = self.bot.get_channel('445673668337729536')
        b = await self.bot.get_message(a, "445898320272162816")
        c = b.content.split(",")
        count = 0
        fmt = "[{0}]  --> {1}"
        for i in c:
            if i == "":
                pass
            else:
                await self.bot.say(fmt.format(count, i))
                count += 1
        if b.content == "":
            await self.bot.say("There are no states saved")

    @commands.command()
    async def rset(self, msg):
        list = "0123456789"
        if msg not in list:
            await self.bot.say("Choose a number from $rstate")
        else:
            a = self.bot.get_channel('445673668337729536')
            b = await self.bot.get_message(a, "445898320272162816")
            c = b.content.split(",")
            await self.bot.change_presence(game=Game(name=c[int(msg)]))
            await self.bot.say("State Succesfully changed")

class dbclear():
    def __init__(self, bot):
        self.bot = bot
        self.db = ["r", "i"]

    @commands.command()
    async def dbclear(self, message):
        if message not in self.db:
            pass
        else:
            if message == "r":
                a = self.bot.get_channel('445673668337729536')
                b = await self.bot.get_message(a, "445898320272162816")
                await self.bot.edit_message(b, " ")
            elif message == "i":
                a = self.bot.get_channel('447433983442026506')
                b = await self.bot.get_message(a, "448092251420360704")
                await self.bot.edit_message(b, " ")


class text_curiosities():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def text(self, ctx, *, message):
        b = message.lower().replace("$text", "")
        a = b.split()
        for i in a:
            c = ""
            for k in i:
                c = c + ":regional_indicator_{}:".format(k)
            await self.bot.say(c)
        await self.bot.delete_message(ctx.message)

    @commands.command()
    async def db(self):
        db1 = "Bots are usually hosted in Heroku, because of how heroku works, they are reseted every 24 hours," \
              " databases too. Making imposible to have data preservation within heroku. To solve this the dev used " \
              "a very obscure technique, converted a private discord server into a simple database that works with parsing"
        embed = discord.Embed(title="Database", description="",
                              color=0x4FBCF3)
        embed.add_field(name="Database used for save gamestates", value="m", inline=False)

        embed.add_field(name="Why this kind of database", value=db1, inline=False)
        await self.bot.say(embed=embed)

    @commands.command()
    async def info(self):
        embed = discord.Embed(title="Extra Functionalities", description="Extra Fuctionalities the bot has",
                                  color=0x4FBCF3)
        embed.add_field(name="Autorole", value="Bot will give automatically upon entrance the role 'Friend/Tester'",
                            inline=False)
        embed.add_field(name="Automessage", value="Bot will send automatically upon entrance a welcome message",
                            inline=False)
        await self.bot.say(embed=embed)

    @commands.command()
    async def fact(self, msg):
        r = requests.get('http://numbersapi.com/{}'.format(msg))
        await self.bot.say(r.text)

    @commands.command()
    async def year(self, msg):
        r = requests.get("http://numbersapi.com/{}/year".format(msg))
        await self.bot.say(r.text)

    @commands.command(pass_context=True)
    async def getrole(self, ctx, *msg):
        a = " ".join(msg)
        role = find(lambda r: r.name.lower() == a, ctx.message.author.server.roles)
        if role:
            await self.bot.add_roles(ctx.message.author, role)
            await self.bot.say("Gotcha boi, you now have {} role".format(role))
        else:
            await self.bot.say("Either the role was misspelled or you are trying to get roles that only admins should have, get rekt kid")

    @commands.command(pass_context=True)
    async def removerole(self, ctx, *msg):
        a = " ".join(msg)
        role = find(lambda r: r.name.lower() == a, ctx.message.author.server.roles)
        if role:
            await self.bot.remove_roles(ctx.message.author, role)
            await self.bot.say("Gotcha boi, you are no longar a {} ".format(role))
        else:
            await self.bot.say("Code just broke probably causing the death of an insane amount of innocent people, don't misspell next time dude ")

    @commands.command()
    async def name(self, new_name: str, user: discord.Member):
        if user and new_name:
            await self.bot.change_nickname(user, new_name)

class loader():
    def __init__(self, bot):
        self.bot = bot

    @commands.check(is_a)
    @commands.command()
    async def load(self, ext):
        self.bot.load_extension(ext)

        await self.bot.say("Loaded extension {}".format(ext))

    @commands.check(is_a)
    @commands.command()
    async def unload(self,ext):
        self.bot.unload_extension(ext)
        await self.bot.say("Unloaded extension {}".format(ext))


def setup(bot):
    bot.add_cog(id_perms(bot))
    bot.add_cog(loader(bot))
    bot.add_cog(sState(bot))
    bot.add_cog(dbclear(bot))
    bot.add_cog(text_curiosities(bot))
