import discord
from discord.utils import find


class Events():
    def __init__(self, bot):
        self.bot = bot

    async def on_message(self, message):
        if message.content.lower().startswith("gay"):
            await self.bot.send_message(message.channel, "Did ya call me")

        elif message.content.startswith("$help"):
            embed = discord.Embed(title="List of commands", description="Note that only $ can be used to call commands",
                                  color=0x4FBCF3)
            embed.add_field(name="$help", value="Shows this", inline=False)
            embed.add_field(name="$db", value="shows info about database used", inline=False)
            embed.add_field(name="$dbrclear", value="restarts (r) database", inline=False)
            embed.add_field(name="$rsave", value="Used to save a desired Bot Game State", inline=False)
            embed.add_field(name="$rstate", value="Used to see what Bot Game States are saved ", inline=False)
            embed.add_field(name="$rset number",
                            value="Used to set a Bot Game State, choose from $rstate number is index in $rstate",
                            inline=False)
            embed.add_field(name="$text <text>", value="Transform text into cool Discord letters", inline=False)
            embed.add_field(name="$getrole <role>", value="If the role is misspelled or admin's, It'll not work")
            embed.add_field(name="$remove<role>", value="If the role is misspelled or admin's, It'll not work")
            embed.add_field(name="$repl",
                            value="Used to execute python code, can only be used by dev for now for anti-trolls reasons",
                            inline=False)
            embed.add_field(name="$info", value="Tells you extra functionalities the bot has")
            embed.add_field(name="$idea <text>",
                            value="Sends text to bot developer, bot functionalities ideas/needs preferable")
            await self.bot.send_message(message.channel, embed=embed)
        elif message.content.startswith("$idea"):
            channel = discord.Object("445977601778122782")
            a = message.content.replace("$idea", "")
            msg = "{0.author.mention}-------- {1}".format(message, a)
            await self.bot.send_message(channel, msg)
        elif message.content.startswith('$cool'):
            await self.bot.send_message(message.channel, 'Who is cool? Type $name namehere')

            def check(msgh):
                return msgh.content.startswith('chocolate')

            message = await self.bot.wait_for_message(author=message.author, check=check)
            name = message.content.replace("chocolate", "")
            await self.bot.send_message(message.channel, '{} is cool indeed'.format(name))

    async def on_member_join(self, member):
        channel = discord.Object("234261267601162240")
        role = find(lambda r: r.name.lower() == 'normie', member.server.roles)
        if role:
            await self.bot.add_roles(member, role)
            await self.bot.send_message(channel, "Welcome {0} to {1}".format(member.mention, member.server.name))

def setup(bot):
    bot.add_cog(Events(bot))
