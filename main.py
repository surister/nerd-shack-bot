import os
import time
from time import time

from discord.ext.commands import Bot



Bot = Bot(command_prefix='$')

@Bot.event
async def on_ready():
    before = time()
    Bot.remove_command('help')
    Bot.load_extension("commands")
    Bot.load_extension("events")
    after = time()
    total_time = after - before
    fmt = "Conectado como: {0} \n Id: {1} \n It took to init the bot {2}".format(Bot.user.name, Bot.user.id, total_time)
    print("----------------------------\n", fmt, "\n----------------------------\n")


Bot.run(os.environ['token'])